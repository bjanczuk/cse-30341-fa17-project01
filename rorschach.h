// Bart Janczuk
// Project 01 Function Headers

#ifndef RORSCHACH_H
#define RORSCHACH_H

extern int INTERVAL;
extern char INPUT_ROOT[];
extern char *ROOT;

#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <ctime>
#include <csignal>
#include <string>
#include <map>
#include <unordered_map>
#include <vector>

#include <stdio.h>
#include <stdlib.h>
#include <libgen.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <dirent.h>
#include <string.h>
#include <unistd.h>
#include <fnmatch.h>

using namespace std;

void usage(int);
void trapSignal(int);
void parseCommandLine(int, char *[]);
void createRules();
void checkFlagArgument(int, int, char *[]);
void scanDirectory(const char *);
bool isCurrentOrParent(string);
char * getFullPath(const char *, char []);
char * getShortPath(const char *);
void compareFiles();
void allCreated();
void allDeleted();
void detected(string, string);
void checkForRuleMatch(string, string);
void executeAction(string, string, string);
void setEnvironmentals(string, string);
void incrementIterators(map<string, time_t>::iterator &, map<string, time_t>::iterator &);

#endif