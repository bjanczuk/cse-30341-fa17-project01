// Bart Janczuk
// Project 01 Functions

#include "rorschach.h"

// Declare default variables
int INTERVAL = 5000000;
string RULES_PATH = "rules";

char INPUT_ROOT[BUFSIZ];
char *ROOT;

map<string, time_t> OLD_FILES;
map<string, time_t> CURRENT_FILES;

unordered_map<string, vector<pair<string, string>>> RULES_MAP;

// Buffers used for globally storing the environmental variables
char base[BUFSIZ];
char full[BUFSIZ];
char ev[BUFSIZ];
char ts[BUFSIZ];

bool FIRST_SCAN = true;

void usage(int status) {
	cout << "Usage: rorschach [options] ROOT\n\n";

	cout << "Options:\n"
	"	-h          Print this help message\n"
	"	-f RULES    Load rules from this file (default is rules)\n"
	"	-t SECONDS  Time between scans (default is 5 seconds)\n";

	if (status == 0) { exit(EXIT_SUCCESS); }
	else { exit(EXIT_FAILURE); }
}

void trapSignal(int signal) {
	cout << endl << "Received SIGINT..." << endl;

	// Free any resources that were malloc'ed and not freed yet
	cout << "Cleaning up..." << endl;
	free(ROOT);

	cout << "Good-bye!" << endl;
	exit(0);
}

void parseCommandLine(int argc, char *argv[]) {
	int current = 1;
	while (current < argc && argv[current][0] == '-') { // Loop as long as the current argument is a flag
		switch (argv[current][1]) {
			case 'h':
				usage(0);
			case 'f':
				current++;
				checkFlagArgument(current, argc, argv);
				RULES_PATH = argv[current];
				break;
			case 't':
				current++;
				checkFlagArgument(current, argc, argv);
				INTERVAL = stoi(argv[current]) * 1000000;
				break;
			default: // Invalid flag was used
				usage(-1);
		}
		current++;
	}

	if (current == argc) { // No ROOT argument was provided
		ROOT = realpath(".", ROOT);
		strcpy(INPUT_ROOT, ".");
	}
	else if (current == argc - 1) { // One more argument remains, so it must be the ROOT argument
		ROOT = realpath(argv[current], ROOT);
		strcpy(INPUT_ROOT, argv[current]);
	}
	else { // Otherwise an invalid number of arguments was used
		usage(-1);
	}

	// Make sure the directory is valid
	if (ROOT == NULL) {
		fprintf(stderr, "Invalid root directory %s: %s\n", argv[current], strerror(errno));
		free(ROOT);
		exit(EXIT_FAILURE);
	}
}

void createRules() {
	string line, event, pattern, action;
	ifstream rules(RULES_PATH);

	// Instantiate the map with empty vectors
	vector<pair<string, string>> temp;
	RULES_MAP.insert(make_pair("CREATE", temp));
	RULES_MAP.insert(make_pair("DELETE", temp));
	RULES_MAP.insert(make_pair("MODIFY", temp));

	if (!rules.is_open()) {
		fprintf(stderr, "Unable to open rules file %s: %s\n", RULES_PATH.c_str(), strerror(errno));
		exit(EXIT_FAILURE);
	}

	while (getline(rules, line)) {
		// Skip empty lines and lines that don't contain all 3 pieces of a rule
		if (line.length() == 0) { continue; }
		stringstream ss(line);
		ss >> event;
		if (ss.fail()) { continue; }
		ss >> pattern;
		if (ss.fail()) { continue; }
		ss >> ws; // Consume the leading whitespace
		if (ss.fail()) { continue; }
		getline(ss, action);
		if (ss.fail()) { continue; }

		// Make sure each rule is a valid one
		if (event.compare("CREATE") == 0 || event.compare("DELETE") == 0 || event.compare("MODIFY") == 0) {
			RULES_MAP[event].push_back(make_pair(pattern, action));
		}
		else {
			fprintf(stderr, "Invalid rule: %s\n\n", line.c_str());
			exit(EXIT_FAILURE);
		}
	}
	rules.close();
}

// Make sure a valid argument follows a flag
void checkFlagArgument(int current, int argc, char *argv[]) {
	if (current >= argc || argv[current][0] == '-') {
		usage(-1);
	}
}

// Starting with the root directory, recursively walk every directory in this directory tree
void scanDirectory(const char *dir) {
	DIR *d = opendir(dir);
	
	if (d == NULL) {
		fprintf(stderr, "Unable to opendir %s: %s\n", dir, strerror(errno));
		exit(EXIT_FAILURE);
	}
	
	struct dirent *e;
	struct stat fs;
	while ((e = readdir(d))) {
		if (!isCurrentOrParent(e->d_name)) { // Skip the '.' and '..' directories
			char *full_path = getFullPath(dir, e->d_name);

			if (stat(full_path, &fs)) {
				fprintf(stderr, "Unable to stat %s: %s\n", full_path, strerror(errno));
				continue;
			}

			// Recurse into a new directory whenever one is found
			if (S_ISDIR(fs.st_mode)) {
				scanDirectory(full_path);
			}
			else { // Otherwise a file was found, so add it to the map
				CURRENT_FILES.insert(make_pair(full_path, fs.st_mtime));
				if (FIRST_SCAN) {
					char * short_path = getShortPath(full_path);
					cout << "Detected \"CREATE\" event on \"" << short_path << "\"" << endl;
					free(short_path); // Deallocate malloc'ed memory
				}
			}
			free(full_path); // // Deallocate malloc'ed memory
		}
	}
	closedir(d);
}

bool isCurrentOrParent(string dir) {
	return (dir.compare(".") == 0 || dir.compare("..") == 0);
}

char * getFullPath(const char *path, char name[]) {
	// Allocate memory for a new char * so it exists outside of the function
	char *new_path = (char *)malloc(BUFSIZ);
	strcpy(new_path, path);

	// Concatenate a slash and the current file to the old path to get the full path
	const char *new_path_end = name;
	strcat(new_path, "/");
	strcat(new_path, new_path_end);
	return new_path;
}

char * getShortPath(const char *path) {
	// Allocate memory for a new char * so it exists outside of the function
	char *short_path = (char *)malloc(BUFSIZ);
	string full(path);
	string rt(ROOT);

	// If the fullpath equivalent of the inputted path is in the given path, replace it with the inputted path
	if (full.find(rt) == 0) {
		strcpy(short_path, INPUT_ROOT);
		strcat(short_path, full.substr(rt.length()).c_str());
    	return short_path;
	}
	strcpy(short_path, path);
	return short_path;
}

void compareFiles() {
	if (!FIRST_SCAN) { // Don't do any rule checking on the very first scan
		auto old_it = OLD_FILES.begin();
		auto current_it = CURRENT_FILES.begin();

		// Handle the cases where only one of the maps is empty to begin with
		if (OLD_FILES.size() == 0 && CURRENT_FILES.size() > 0) {
			allCreated();
		}
		else if (OLD_FILES.size() > 0 && CURRENT_FILES.size() == 0) {
			allDeleted();
		}
		else { // Otherwise use a while loop to compare the maps file-by-file
			while (old_it != OLD_FILES.end() && current_it != CURRENT_FILES.end()) {
				if ((old_it->first).compare(current_it->first) != 0) { // A file has either been added or removed, because the file names (keys) don't match up
					if (OLD_FILES.find(current_it->first) == OLD_FILES.end()) { // A file was added
						detected("CREATE", current_it->first);
					}
					else { // A file was removed
						detected("DELETE", old_it->first);
					}
				}
				else { // The file names are match, so check if the file's modification time has changed
					if (old_it->second < current_it->second) {
						detected("MODIFY", current_it->first);
					}
				}
				incrementIterators(old_it, current_it);
			}
		}
	}
	FIRST_SCAN = false;

	// Once all the files have been compared, move the current files to the OLD_FILE map and clear them from the current map
	OLD_FILES.clear();
	OLD_FILES.insert(CURRENT_FILES.begin(), CURRENT_FILES.end());
	CURRENT_FILES.clear();
}

// Every file is new, so process every file in the current map
void allCreated() {
	auto it = CURRENT_FILES.begin();

	while (it != CURRENT_FILES.end()) {
		detected("CREATE", it->first);
		it++;
	}
}

// Every file was deleted, so process every file in the old map
void allDeleted() {
	auto it = OLD_FILES.begin();

	while (it != OLD_FILES.end()) {
		detected("DELETE", it->first);
		it++;
	}
}

void detected(string type, string path) {
	char * short_path = getShortPath(path.c_str());
	cout << "Detected \"" << type << "\" event on \"" << short_path << "\"" << endl;
	checkForRuleMatch(type, short_path);
	free(short_path);
}

void checkForRuleMatch(string event, string path) {
	if (RULES_MAP.find(event) == RULES_MAP.end()) { // Make sure the event passed in is valid
		return;
	}

	// Look through every rule for the given event to see if a pattern matches the current path
	for (auto it = RULES_MAP[event].begin(); it != RULES_MAP[event].end(); it++) {
		if (fnmatch((it->first).c_str(), basename((char *)path.c_str()), 0) == 0) {
			cout << "Matched pattern \"" << (it->first) << "\" on \"" << path << "\"" << endl;
			executeAction(event, path, it->second);
		}
	}
}

void executeAction(string event, string path, string action) {
	pid_t pid;
	int status;

	setEnvironmentals(event, path);

	if ((pid = fork()) < 0) { // Fork failed
		fprintf(stderr, "Fork failed: %s\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	else if (pid == 0) { // Child
		char command[BUFSIZ] = {0};
		strcpy(command, action.c_str());

		cout << "Executing action \"" << action << "\" on " << path << endl;

		if (execl("/bin/sh", "/bin/sh", "-c", command, NULL)) { // Execute the command as a shell command
			fprintf(stderr, "execvp failed: %s\n", strerror(errno));
			exit(EXIT_FAILURE);
		}
	}
	else { // Parent
		if (waitpid(pid, &status, 0) != pid) {
			exit(EXIT_FAILURE);
		}
	}
}

void setEnvironmentals(string event, string path) {
	// Create the string for each environmental variable declaration and then call putenv() on it
	sprintf(base, "BASEPATH=%s", basename((char *)path.c_str()));
	putenv(base);

	sprintf(full, "FULLPATH=%s", (char *)path.c_str());
	putenv(full);

	sprintf(ev, "EVENT=%s", (char *)event.c_str());
	putenv(ev);

	time_t timestamp;
	sprintf(ts, "%s", (char *)("TIMESTAMP=" + to_string(time(&timestamp))).c_str());
	putenv(ts);
}

// Check which iterator(s) should be advanced
void incrementIterators(map<string, time_t>::iterator &old_it, map<string, time_t>::iterator &current_it) {
	bool increase_old = false;
	bool increase_current = false;
	
	auto temp = current_it;
	temp++;
	if (old_it->first <= current_it->first || temp == CURRENT_FILES.end()) {
		increase_old = true;
	}

	temp = old_it;
	temp++;
	if (old_it->first >= current_it->first || temp == OLD_FILES.end()) {
		increase_current = true;
	}
	
	if (increase_old) {
		old_it++;
	}
	if (increase_current) {
		current_it++;
	}

	// Make sure both maps get traversed fully by keeping the iterators from reaching the ends separately
	if (old_it == OLD_FILES.end() && current_it != CURRENT_FILES.end()) {
		old_it--;
	}
	else if (old_it != OLD_FILES.end() && current_it == CURRENT_FILES.end()) {
		current_it--;
	}
}