CSE.30341.FA17: Project 01
==========================

This is the documentation for [Project 01] of [CSE.30341.FA17].

[Project 01]:       https://www3.nd.edu/~pbui/teaching/cse.30341.fa17/project01.html
[CSE.30341.FA17]:   https://www3.nd.edu/~pbui/teaching/cse.30341.fa17/

Members
-------

1. Bart Janczuk (bjanczuk@nd.edu)

Design
------

> Explain how you would periodically scan the `root` directory and detect
> if a file has been created, modified, or removed.
>
>   1. What system calls would you need to use?
		In order to read files in a directory, you need to use the "opendir", "readdir", and "closedir" system calls.
		These calls allow you to walk the directory, which you'll need to do each time in order to check whether the files have changed at all.
>
>   2. What information would you need to store and what data structures would
>      you need?
		You'd need to store every file's full path and modification time. A hash table could be used to map files from their path
		to their most recent modification time in order to check for modification. Created or removed files would be determined by checking whether the
		files (AKA keys) in the hash have been changed in general.

.

> Explain how you would check if a rule matched an event and execute the
> corresponding action.
>
>   1. What system calls would you need to use?
		In order to execute an action, you'd need to use the fork()/exec()/wait() group of system calls.
>
>   2. How would you pass the environment variables to the command?
		You would need to use the language's function for setting and getting environmental variables (e.g. in C, you'd use putenv() and getenv()).
.

> Explain how you would handle a [SIGINT] signal and what you would need to
> cleanup?
>
>   1. What system calls would you need to use?
		You'd need to use the signal() and exit() system calls.
>
>   2. How would you know what resources to clean up?
		You'd just need to keep track of everything that'd been malloc'ed prior to that point.

Testing
-------

> Describe how you tested and verified that `rorschach` met the project
> requirements.

First of all, I conducted my own tests. In one window, I ran the program, and in another, I
manually tried random variations of adding, removing, and modifying files to see how the program
reacted. I used this process until the program wasn't blatantly failing. Then, I used the demo
script received from my TA. Upon the first test, the script showed that I was getting a lot of
things wrong still, so I used its results to go through my code and fix the little errors that
I hadn't noticed beforehand. Eventually, everything worked, and I figured that was verification
of meeting the requirements.

Analysis
--------

> How is what `rorschach` does similar to how a [system call] operates?

In the same way that a system call forces the CPU to look up a function in a trap table, `rorschach` - 
when it potentially needs to perform an action - is forced to 1) look up rules in its 'rule table', so
to speak, and then 2) to look up patterns inside those rules.

.

> As described in the project requirements, `rorschach` periodically scans the
> `root` directory to detect changes in files.  This process can be described
> as a form of [busy waiting].
>
>   1. In this specific context, why could this be considered a possible
>      negative design flaw?
		This could be considered a flaw because this means that `rorschach` does
		nothing while waiting. It simply waits, so in effect, it wastes CPU resources
		and blocks other code from running.
>
>   2. Consider [inotify(7)] provided by [Linux].  How would these system calls
>      help address the problem of [busy waiting]?
		Those system calls address busy waiting's problem by getting rid of the "busy"
		part. They check for changes, but they do so without blocking; instead of running
		as an application that periodically checks for changes, the filesystem itself
		notifies applications of changes instead.
>
>   3. Why might we still consider using [busy waiting] as specified in the
>      original design rather than use something like [inotify(7)]?
		For one, busy waiting is more reliable/consistent (since you know when the checks
		will happen). Furthermore, as the inotify manual page says, inotify has drawbacks
		such as an inability to monitor network filesystems, which busy waiting could avoid.

[Linux]:        https://kernel.org
[busy waiting]: https://en.wikipedia.org/wiki/Busy_waiting
[system call]:  https://en.wikipedia.org/wiki/System_call
[inotify(7)]:   http://man7.org/linux/man-pages/man7/inotify.7.html

Errata
------

> Describe any known errors, bugs, or deviations from the requirements.
As far as I can tell, everything works fine.

Extra Credit
------------

> Describe what extra credit (if any) that you implemented.
.

Grader
------

- Kyle Gifaldi (kgifaldi@nd.edu)
