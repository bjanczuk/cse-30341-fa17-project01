// Bart Janczuk
// Project 01 Main File

#include "rorschach.h"

int main (int argc, char *argv[]) {
	// Prepare trapSignal() for the SIGINT signal
	signal(SIGINT, trapSignal); 

	// Process any flags, the ROOT argument, and the rules file
	parseCommandLine(argc, argv);
	createRules();

	cout << "Monitoring " << INPUT_ROOT << endl;

	// Periodically and indefinitely scan the directory tree to check for events
	while (1) {
		scanDirectory(ROOT);
		compareFiles();
		usleep(INTERVAL);
	}
}